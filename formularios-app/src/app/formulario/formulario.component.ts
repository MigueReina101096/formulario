import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  usuario = {
    nombre: '',
    apellido: '',
    email: '',
    telefono: '',
    notificacionTelefono: '',
  }
  formularioRegistro: FormGroup;
  mensajeNombre: any;
  private mensajeValidacion = {
    required: 'El nombre es obligatorio.',
    minLength: 'El nombre debe tener mas de tres(3) caracteres.',
    saludo: 'No cumple con la palabra Hola',
    saludo1: 'No entra con el parametro en la customizada con parametros',
    coincidenciaNombre: 'No se encuentra coincidencia en el nombre',
  }
  mensajesErrorNombreConfirmar: any;
  private mensajesValidacionNombreConfirmar = {
    required: 'El nombre es obligatorio',
  }
  constructor(private readonly _formBuilder: FormBuilder) { }
  ngOnInit() {
    // this.formularioRegistro =  new FormGroup({
    //   nombre: new FormControl(),
    //   apellido: new FormControl(),
    //   email: new FormControl(),
    // })
    // this.formularioRegistro = this._formBuilder.group({
    //   nombre:'',
    //   apellido:'',
    //   email:'',
    // })
    this.formularioRegistro = this._formBuilder.group({
      grupoNombre: this._formBuilder.group({
        nombre: ['', [
          Validators.required,
          Validators.minLength(3),
          this.validacionCustomizada,
          this.validacionCustomizadaConParametro
        ]],
        nombreConfirmacion: ['', [Validators.required]],
      }, 
      { validator: this.validacionGrupoNombre },
      ),
      apellido: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.minLength(3)]],
      telefono: ['', [Validators.maxLength(10)]],
      notificacionTelefono: true,
    })
    this.escucharCambiosNotificacion();
    this.escucharGrupoNombre();
  }
  escucharCambiosNotificacion() {
    const telefonoFormControl$ = this.formularioRegistro.get('notificacionTelefono').valueChanges
    telefonoFormControl$.subscribe((valor) => {
      console.log('notificacion telefono', valor)
      this.cambiarValidacionTelefono(valor)
    })
  }
  escucharCambiosNombre() {
    const nombreFormControl = this.formularioRegistro.get('nombre')
    nombreFormControl.valueChanges.subscribe((valorNombre) => {
      this.setearMensajesNombre(nombreFormControl)
    })
  }
  save() {
    console.log(this.formularioRegistro)
  }
  llenarFormulario() {
    this.formularioRegistro.patchValue({
      nombre: 'prueba1',
      apellido: 'prueba2',
      email: 'correo',
      telefono: '2824064',
    })
    /*  this.formularioRegistro.setValue({nombre:'prueba1',})
      */
  }
  vaciarFormulario() {
    this.formularioRegistro.reset()
  }
  cambiarValidacionTelefono(valor) {
    const controlTelefono = this.formularioRegistro.get('telefono')
    const notificacionTelefono = this.formularioRegistro.get('notificacionTelefono').value
    controlTelefono.setValidators(Validators.required)
    controlTelefono.clearValidators()
    controlTelefono.updateValueAndValidity() // Actualizo y valido
  }
  setearMensajesNombre(valor: AbstractControl) {
    this.mensajeNombre = '';
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if (esValidoCampo) {
      this.mensajeNombre = Object.keys(valor.errors).map((atributo) => {
        this.mensajeValidacion[atributo].join('')
      })
    }
  }
  validacionCustomizada(valor: AbstractControl): { [atributo: string]: boolean } | null {
    if (valor.value != 'Hola') {
      return { 'saludo': true }
    } else {
      return null
    }
  }
  validacionCustomizadaConParametro(palabra: string): ValidatorFn {
    return (valor: AbstractControl): { [atributo: string]: boolean } | null => {
      if (valor.value != 'Hola') {
        return { 'saludo1': true }
      } else {
        return null
      }
    }
  }
  escucharGrupoNombre() {
    const grupoNombre = this.formularioRegistro.get('grupoNombre.nombre').valueChanges
    grupoNombre.subscribe((consulta) => {
      console.log(consulta.nombre)
      console.log(consulta.nombreConfirmacion)
    })
    console.log('Grupo nombre', grupoNombre)
  }
  validacionGrupoNombre(grupoNombre: AbstractControl): ValidatorFn {
    console.log('validacion grupo nombre', grupoNombre)
    return (): { [parametro: string]: boolean } | null => {
      return { 'coincidenciaNombre': true }
    }
  }
  escucharCambiosTelefono(){
    const modificacionTelefono$ = this.formularioRegistro.get('telefono').valueChanges
    modificacionTelefono$
    .subscribe(
      (valor)=>{
        console.log('Esta es la modificacion del telefono: ', valor)
      }
    )
  }
}
